class BreedsController < ApplicationController

  def index
    @breeds = DogBreedFetcher.new().fetch_all_breeds
    @breeds_bond = {}
    @breeds.each do |breed|
      @breeds_bond[breed] = DogBreedFetcher.fetch(breed).pic_url
    end
  end

end
